/*
 * Created with [y] YaaS Service SDK version 4.17.1.
 */
package com.sample.wishlist;

import com.sap.cloud.yaas.servicesdk.jerseysupport.features.BeanValidationFeature;
import com.sap.cloud.yaas.servicesdk.jerseysupport.features.JerseyFeature;
import com.sap.cloud.yaas.servicesdk.jerseysupport.features.JsonFeature;
import com.sap.cloud.yaas.servicesdk.jerseysupport.features.PatchFeature;
import com.sap.cloud.yaas.servicesdk.jerseysupport.features.SecurityFeature;
import com.sap.cloud.yaas.servicesdk.jerseysupport.logging.RequestResponseLoggingFilter;
import com.sap.cloud.yaas.servicesdk.ping.PingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AssignableTypeFilter;
import org.springframework.util.ClassUtils;

import javax.ws.rs.core.Feature;
import java.util.Optional;


/**
 * Defines the REST application.
 */
public class JerseyApplication extends ResourceConfig
{

	private static final Logger LOG = LoggerFactory.getLogger(JerseyApplication.class);

	/**
	 * Initializes the jersey application.
	 */
	public JerseyApplication()
	{
		// enable error responses in JSON format
		register(JerseyFeature.class);

		// enable JSON support
		register(JsonFeature.class);

		// hybris-scopes support for @RolesAllowed
		register(SecurityFeature.class);

		// bean validation support
		register(BeanValidationFeature.class);

		// ping support
		register(PingFeature.class);

		// register resource implementation classes
		registerApiFeature();

		// patch support
		register(PatchFeature.class);

		// log incoming requests
		register(new RequestResponseLoggingFilter(LOG));
	}

	private void registerApiFeature()
	{
		new ApiFeatureClassScanner().getApiFeature().map(f -> register(f));
	}

	/**
	 * Scans the classpath for a class 'ApiFeature' implementing {@link Feature}.
	 */
	private static class ApiFeatureClassScanner extends ClassPathScanningCandidateComponentProvider
	{
		private final static String API_FEATURE = ".ApiFeature";

		public ApiFeatureClassScanner()
		{
			super(false);
			addIncludeFilter(new AssignableTypeFilter(Feature.class));
		}

		/**
		 * Scans the classpath for a class 'ApiFeature' implementing {@link Feature}.
		 *
		 * @return 'ApiFeature' class
		 */
		@SuppressWarnings("unchecked")
		public final Optional<Class<?>> getApiFeature()
		{
			for (final BeanDefinition candidate : findCandidateComponents(getClass().getPackage().getName()))
			{
				try
				{
					if (candidate.getBeanClassName().endsWith(API_FEATURE))
					{
						return Optional
								.of(ClassUtils.resolveClassName(candidate.getBeanClassName(), ClassUtils.getDefaultClassLoader()));
					}
				}
				catch (final IllegalArgumentException e)
				{
					LOG.warn("Could not identify 'ApiFeature' class", e);
				}
			}
			return Optional.empty();
		}

	}
}
