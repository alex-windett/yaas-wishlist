
package com.sample.wishlist.api.impl;

import javax.inject.Singleton;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

/**
* Resource class containing the custom logic. Please put your logic here!
*/
@Component("apiWishlistsResource")
@Singleton
public class DefaultWishlistsResource implements com.sample.wishlist.api.WishlistsResource
{
	@javax.ws.rs.core.Context
	private javax.ws.rs.core.UriInfo uriInfo;

	/* GET / */
	@Override
	public Response get(final com.sample.wishlist.api.param.YaasAwareParameters yaasAware)
	{
		// place some logic here
		return Response.ok()
			.entity(new java.util.ArrayList<com.sample.wishlist.api.dto.Wishlist>()).build();
	}

	/* POST / */
	@Override
	public Response post(final com.sample.wishlist.api.param.YaasAwareParameters yaasAware, final com.sample.wishlist.api.dto.Wishlist wishlist)
	{
		// place some logic here
		return Response.created(uriInfo.getAbsolutePath())
			.build();
	}

	/* GET /{wishlistId} */
	@Override
	public Response getByWishlistId(final com.sample.wishlist.api.param.YaasAwareParameters yaasAware, final java.lang.String wishlistId)
	{
		// place some logic here
		return Response.ok()
			.entity(new com.sample.wishlist.api.dto.Wishlist()).build();
	}

	/* PUT /{wishlistId} */
	@Override
	public Response putByWishlistId(final com.sample.wishlist.api.param.YaasAwareParameters yaasAware, final java.lang.String wishlistId, final com.sample.wishlist.api.dto.Wishlist wishlist)
	{
		// place some logic here
		return Response.ok()
			.build();
	}

	/* DELETE /{wishlistId} */
	@Override
	public Response deleteByWishlistId(final com.sample.wishlist.api.param.YaasAwareParameters yaasAware, final java.lang.String wishlistId)
	{
		// place some logic here
		return Response.noContent()
			.build();
	}

	/* GET /{wishlistId}/wishlistItems */
	@Override
	public Response getByWishlistIdWishlistItems(final com.sample.wishlist.api.param.YaasAwareParameters yaasAware, final java.lang.String wishlistId)
	{
		// place some logic here
		return Response.ok()
			.entity(new java.util.ArrayList<com.sample.wishlist.api.dto.WishlistItem>()).build();
	}

	/* POST /{wishlistId}/wishlistItems */
	@Override
	public Response postByWishlistIdWishlistItems(final com.sample.wishlist.api.param.YaasAwareParameters yaasAware, final java.lang.String wishlistId, final com.sample.wishlist.api.dto.WishlistItem wishlistItem)
	{
		// place some logic here
		return Response.created(uriInfo.getAbsolutePath())
			.entity("myEntity").build();
	}

}
