package com.sample.wishlist.api.util;



import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;


/**
 * Generic utility class used for deserialization of oneof constructs.
 */
public final class OneOfDeserializerUtility
{
	private static final Logger LOG = LoggerFactory.getLogger(OneOfDeserializerUtility.class);

	/**
	 * Hidden constructor to avoid creation.
	 */
	private OneOfDeserializerUtility()
	{
		//avoid creation
	}

	/**
	 * Reads the next value from given JSON parser assuring that the value is one of the defined types.
	 *
	 * @param jsonParser     parser to read the value from
	 * @param supportedTypes to one of the types the value will be converted
	 * @return the converted value
	 * @throws IOException in case of problems reading the next value from json parser or if the value is not convertable
	 *                     to any of the types
	 */
	public static Object deserialize(final JsonParser jsonParser, final Class<?>[] supportedTypes) throws IOException
	{
		final ObjectMapper objectMapper = (ObjectMapper) jsonParser.getCodec();
		final JsonNode node = jsonParser.readValueAsTree();
		if (node == null)
		{
			throw new JsonMappingException("Could not find valid mapping for empty JSON value.");
		}

		final String nodeAsString = getNodeAsText(objectMapper, node);
		return readAsPrimitiveIfPossible(node, nodeAsString, Arrays.asList(supportedTypes), objectMapper);
	}

	/**
	 * Tries to interpret the given node as one of possible primitives.
	 * <p/>
	 * <ul>
	 * <li>boolean</li>
	 * <li>integer</li>
	 * <li>number</li>
	 * <li>string</li>
	 * </ul>
	 * <p/>
	 * If it fails to find the matching primitive type then fallbacks to eager default mapping
	 *	{@link #readAsSupportedType(java.util.Collection, com.fasterxml.jackson.databind.ObjectMapper, String)}.
	 */
	private static Object readAsPrimitiveIfPossible(final JsonNode node, final String nodeAsString,
			final Collection<Class<?>> supportedTypes, final ObjectMapper objectMapper)
			throws IOException, IllegalArgumentException
	{
		final Class<?> type = retrieveSupportedNodeType(node, supportedTypes);
		if (type != null)
		{
			return objectMapper.readValue(nodeAsString, type);
		}
		else
		{
			return readAsSupportedType(supportedTypes, objectMapper, nodeAsString);
		}
	}

	private static Class<?> retrieveSupportedNodeType(final JsonNode node, final Collection<Class<?>> supportedTypes)
	{
		if (isSupportedDoubleType(node, supportedTypes))
		{
			return Double.class;
		}
		else if (isSupportedIntegerType(node, supportedTypes))
		{
			return Integer.class;
		}
		else if (isSupportedNumberType(node, supportedTypes))
		{
			return Number.class;
		}
		else if (isSupportedBooleanType(node, supportedTypes))
		{
			return Boolean.class;
		}
		else if (isSupportedStringType(node, supportedTypes))
		{
			return String.class;
		}
		return null;
	}

	private static boolean isSupportedStringType(final JsonNode node, final Collection<Class<?>> supportedTypes)
	{
		return node.isTextual() && supportedTypes.contains(String.class);
	}

	private static boolean isSupportedBooleanType(final JsonNode node, final Collection<Class<?>> supportedTypes)
	{
		return node.isBoolean() && supportedTypes.contains(Boolean.class);
	}

	private static boolean isSupportedNumberType(final JsonNode node, final Collection<Class<?>> supportedTypes)
	{
		return node.isNumber() && supportedTypes.contains(Number.class);
	}

	private static boolean isSupportedIntegerType(final JsonNode node, final Collection<Class<?>> supportedTypes)
	{
		return node.isInt() && supportedTypes.contains(Integer.class);
	}

	private static boolean isSupportedDoubleType(final JsonNode node, final Collection<Class<?>> supportedTypes)
	{
		return node.isDouble() && supportedTypes.contains(Double.class);
	}

	private static Object readAsSupportedType(final Collection<Class<?>> supportedTypes, final ObjectMapper objectMapper,
			final String nodeAsString) throws IOException
	{
		for (final Class<?> tryClass : supportedTypes)
		{
			try
			{
				return objectMapper.readValue(nodeAsString, tryClass);
			}
			catch (final JsonProcessingException e)
			{
				LOG.debug("Failed to read '" + nodeAsString + "' as " + tryClass + ".");
			}
		}
		throw new JsonMappingException("Could not deserialize " + nodeAsString + " as one of " + Arrays.asList(supportedTypes));
	}

	private static String getNodeAsText(final ObjectMapper objectMapper, final JsonNode node) throws JsonProcessingException
	{
		final Object nodeAsObject = objectMapper.treeToValue(node, Object.class);
		return objectMapper.writeValueAsString(nodeAsObject);
	}
}
