package com.sample.wishlist.api.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Generated dto.
 */
@javax.annotation.Generated(value = "hybris", date = "Thu Jan 18 21:48:53 GMT 2018")
@XmlRootElement
@JsonAutoDetect(isGetterVisibility = Visibility.NONE,
    getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE,
    creatorVisibility = Visibility.NONE, fieldVisibility = Visibility.NONE)
public class Detail
{

	@com.fasterxml.jackson.annotation.JsonProperty(value="field")
	private java.lang.String field;

	@com.fasterxml.jackson.annotation.JsonProperty(value="type")
	@javax.validation.constraints.Pattern(regexp="[a-z]+[a-z_]*[a-z]+")
	@javax.validation.constraints.NotNull
	private java.lang.String type;

	@com.fasterxml.jackson.annotation.JsonProperty(value="message")
	private java.lang.String message;

	@com.fasterxml.jackson.annotation.JsonProperty(value="moreInfo")
	private java.net.URI moreInfo;

	public java.lang.String getField()
	{
		return field;
	}

	public java.lang.String getType()
	{
		return type;
	}

	public java.lang.String getMessage()
	{
		return message;
	}

	public java.net.URI getMoreInfo()
	{
		return moreInfo;
	}

	public void setField(final java.lang.String field)
	{
		this.field = field;
	}

	public void setType(final java.lang.String type)
	{
		this.type = type;
	}

	public void setMessage(final java.lang.String message)
	{
		this.message = message;
	}

	public void setMoreInfo(final java.net.URI moreInfo)
	{
		this.moreInfo = moreInfo;
	}

}
