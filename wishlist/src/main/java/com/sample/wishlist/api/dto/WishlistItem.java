package com.sample.wishlist.api.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Generated dto.
 */
@javax.annotation.Generated(value = "hybris", date = "Thu Jan 18 21:48:53 GMT 2018")
@XmlRootElement
@JsonAutoDetect(isGetterVisibility = Visibility.NONE,
    getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE,
    creatorVisibility = Visibility.NONE, fieldVisibility = Visibility.NONE)
public class WishlistItem
{

	@com.fasterxml.jackson.annotation.JsonProperty(value="product")
	@javax.validation.constraints.Pattern(regexp="^.+")
	@javax.validation.constraints.NotNull
	private java.lang.String product;

	@com.fasterxml.jackson.annotation.JsonProperty(value="amount")
	@javax.validation.constraints.DecimalMin(value="1")
	@javax.validation.constraints.NotNull
	private java.lang.Integer amount;

	@com.fasterxml.jackson.annotation.JsonProperty(value="note")
	private java.lang.String note;

	@com.fasterxml.jackson.annotation.JsonProperty(value="createdAt")
	private java.util.Date createdAt;

	public java.lang.String getProduct()
	{
		return product;
	}

	public java.lang.Integer getAmount()
	{
		return amount;
	}

	public java.lang.String getNote()
	{
		return note;
	}

	public java.util.Date getCreatedAt()
	{
		return createdAt;
	}

	public void setProduct(final java.lang.String product)
	{
		this.product = product;
	}

	public void setAmount(final java.lang.Integer amount)
	{
		this.amount = amount;
	}

	public void setNote(final java.lang.String note)
	{
		this.note = note;
	}

	public void setCreatedAt(final java.util.Date createdAt)
	{
		this.createdAt = createdAt;
	}

}
