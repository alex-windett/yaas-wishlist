package com.sample.wishlist.api.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Generated dto.
 */
@javax.annotation.Generated(value = "hybris", date = "Thu Jan 18 21:48:53 GMT 2018")
@XmlRootElement
@JsonAutoDetect(isGetterVisibility = Visibility.NONE,
    getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE,
    creatorVisibility = Visibility.NONE, fieldVisibility = Visibility.NONE)
public class Error
{

	@com.fasterxml.jackson.annotation.JsonProperty(value="status")
	@javax.validation.constraints.DecimalMin(value="100")
	@javax.validation.constraints.DecimalMax(value="599")
	@javax.validation.constraints.NotNull
	private java.lang.Integer status;

	@com.fasterxml.jackson.annotation.JsonProperty(value="type")
	@javax.validation.constraints.Pattern(regexp="[a-z]+[a-z_]*[a-z]+")
	@javax.validation.constraints.NotNull
	private java.lang.String type;

	@com.fasterxml.jackson.annotation.JsonProperty(value="message")
	private java.lang.String message;

	@com.fasterxml.jackson.annotation.JsonProperty(value="moreInfo")
	private java.net.URI moreInfo;

	@com.fasterxml.jackson.annotation.JsonProperty(value="details")
	@javax.validation.Valid
	private java.util.List<com.sample.wishlist.api.dto.Detail> details;

	public java.lang.Integer getStatus()
	{
		return status;
	}

	public java.lang.String getType()
	{
		return type;
	}

	public java.lang.String getMessage()
	{
		return message;
	}

	public java.net.URI getMoreInfo()
	{
		return moreInfo;
	}

	public java.util.List<com.sample.wishlist.api.dto.Detail> getDetails()
	{
		return details;
	}

	public void setStatus(final java.lang.Integer status)
	{
		this.status = status;
	}

	public void setType(final java.lang.String type)
	{
		this.type = type;
	}

	public void setMessage(final java.lang.String message)
	{
		this.message = message;
	}

	public void setMoreInfo(final java.net.URI moreInfo)
	{
		this.moreInfo = moreInfo;
	}

	public void setDetails(final java.util.List<com.sample.wishlist.api.dto.Detail> details)
	{
		this.details = details;
	}

}
